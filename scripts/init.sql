-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 28 Octobre 2015 à 18:11
-- Version du serveur :  5.6.27-log
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `prog`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonce`
--

CREATE TABLE IF NOT EXISTS `annonce` (
  `id_ann` int(11) NOT NULL AUTO_INCREMENT,
  `titre_ann` varchar(128) NOT NULL,
  `desc_ann` varchar(128) NOT NULL,
  `image_ann` varchar(128) NOT NULL,
  `id_qua` int(11) NOT NULL,
  `superficie_ann` int(11) NOT NULL,
  `prix_ann` int(11) NOT NULL,
  `id_ville` int(11) NOT NULL,
  `id_ven` int(11) NOT NULL,
  `id_typ` int(11) NOT NULL,
  `nbpiece_ann` int(11) NOT NULL,
  `date_ann` date NOT NULL,
  `loc_ann` varchar(128) NOT NULL,
  PRIMARY KEY (`id_ann`),
  KEY `id_ville` (`id_ville`),
  KEY `id_qua` (`id_qua`),
  KEY `id_ven` (`id_ven`),
  KEY `id_typ` (`id_typ`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `annonce`
--

INSERT INTO `annonce` (`id_ann`, `titre_ann`, `desc_ann`, `image_ann`, `id_qua`, `superficie_ann`, `prix_ann`, `id_ville`, `id_ven`, `id_typ`, `nbpiece_ann`, `date_ann`, `loc_ann`) VALUES
(1, 'Maison plein pied', 'superbe maison pour les retraités', 'images/maison.jpg', 1, 100, 1234, 1, 1, 1, 7, '2015-10-20', 'vente'),
(2, 'Appart', 'superbe appart pour les étudiants', 'images/appart.jpg', 4, 50, 600, 2, 2, 2, 3, '2015-10-20', 'location');

-- --------------------------------------------------------

--
-- Structure de la table `quartier`
--

CREATE TABLE IF NOT EXISTS `quartier` (
  `id_qua` int(11) NOT NULL AUTO_INCREMENT,
  `nom_qua` varchar(128) NOT NULL,
  `id_vil` int(11) NOT NULL,
  PRIMARY KEY (`id_qua`),
  KEY `id_vil` (`id_vil`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `quartier`
--

INSERT INTO `quartier` (`id_qua`, `nom_qua`, `id_vil`) VALUES
(1, 'gare', 1),
(2, 'Charlemagne', 1),
(3, 'Jeanne d arc', 2),
(4, 'Foch', 2);

-- --------------------------------------------------------

--
-- Structure de la table `type_bien`
--

CREATE TABLE IF NOT EXISTS `type_bien` (
  `id_typ` int(11) NOT NULL AUTO_INCREMENT,
  `nom_typ` varchar(128) NOT NULL,
  PRIMARY KEY (`id_typ`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `type_bien`
--

INSERT INTO `type_bien` (`id_typ`, `nom_typ`) VALUES
(1, 'Maison'),
(2, 'Appartement'),
(3, 'Immeuble');

-- --------------------------------------------------------

--
-- Structure de la table `vendeur`
--

CREATE TABLE IF NOT EXISTS `vendeur` (
  `id_ven` int(11) NOT NULL AUTO_INCREMENT,
  `numTel_ven` int(10) NOT NULL,
  `adresse_ven` varchar(128) NOT NULL,
  `mail_adr` varchar(128) NOT NULL,
  `nom_ven` varchar(128) NOT NULL,
  `prenom_ven` varchar(128) NOT NULL,
  `type_ven` varchar(128) NOT NULL,
  `pass_ven` text NOT NULL,
  PRIMARY KEY (`id_ven`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `vendeur`
--

INSERT INTO `vendeur` (`id_ven`, `numTel_ven`, `adresse_ven`, `mail_adr`, `nom_ven`, `prenom_ven`, `type_ven`, `pass_ven`) VALUES
(1, 502030405, '55 palais royal Nancy', 'vendeur1@gmail.fr', 'Cordier', 'David', 'particulier', 'test1'),
(2, 670256545, '1 bld leopold', 'vendeur2@gmail.fr', 'Rimet', 'Anthony', 'agence', 'test2');

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

CREATE TABLE IF NOT EXISTS `ville` (
  `id_vil` int(11) NOT NULL AUTO_INCREMENT,
  `nom_vil` varchar(80) NOT NULL,
  PRIMARY KEY (`id_vil`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `ville`
--

INSERT INTO `ville` (`id_vil`, `nom_vil`) VALUES
(1, 'Nancy'),
(2, 'Commercy');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `annonce`
--
ALTER TABLE `annonce`
  ADD CONSTRAINT `annonce_ibfk_1` FOREIGN KEY (`id_ville`) REFERENCES `ville` (`id_vil`),
  ADD CONSTRAINT `annonce_ibfk_2` FOREIGN KEY (`id_qua`) REFERENCES `quartier` (`id_qua`),
  ADD CONSTRAINT `annonce_ibfk_3` FOREIGN KEY (`id_ven`) REFERENCES `vendeur` (`id_ven`),
  ADD CONSTRAINT `annonce_ibfk_4` FOREIGN KEY (`id_typ`) REFERENCES `type_bien` (`id_typ`);

--
-- Contraintes pour la table `quartier`
--
ALTER TABLE `quartier`
  ADD CONSTRAINT `quartier_ibfk_1` FOREIGN KEY (`id_vil`) REFERENCES `ville` (`id_vil`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
