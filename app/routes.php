<?php

$app->get('/', function () use ($app) {
  $app->render('accueil.twig');
});


$app->get('/annonces', function () use ($app) {
  $annonces = Annonce::with('type','ville','quartier')->orderBy('date_ann','DESC')->get();
  $app->render('annonces.twig', array('annonces' => $annonces));
});


$app->get('/annonce:num', function ($num) use ($app) {
  $annonce = Annonce::with('type','ville','vendeur','quartier')->where('id_ann', '=', $num)->get();
  $app->render('annonce.twig', array('annonce' => $annonce));
});

$app->post('/supprimer', function () use ($app) {
include('controlleur/supprimer.php');
});

$app->post('/mail', function () use ($app) {
  $mail = $app->request->post('mail');
  $titre = $app->request->post('titre_ann');
  $envoi=$app->request->post('envoi');
  $envoi_mail=array('mail' => $mail,'titre' => $titre);
  if (!isset($envoi) ){
    $app->render('mail.twig', array('mail' => $envoi_mail));
  }else{
    include('controlleur/mail.php');
    $app->render('mail_envoi.twig', array('envoi' => $envoie));

  }
});

$app->get('/recherche', function () use ($app) {
  include('controlleur/recherche.php');
  $app->render('recherche.twig', $recherche);
});

$app->post('/recherche', function () use ($app) {
  include('controlleur/recherche_rez.php');
  $app->render('recherche_rez.twig',array('rez' => $recherche_rez));

});

$app->get('/ajout', function () use ($app) {
  $type = Type::all();
  $app->render('ajout.twig', array('type'=> $type));

});

$app->post('/ajout', function () use ($app) {
  include('controlleur/ajout.php');
  $app->render('ajout2.twig');

});


?>
