<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Type extends Eloquent {
  protected $table = 'type_bien';
  protected $primaryKey='id_typ';
  public $timestamps = false;
  public function annonce()
  {
    return $this->hasMany('Annonce','id_ann');
  }

}

?>
