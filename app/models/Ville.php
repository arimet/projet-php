<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Ville extends Eloquent {
  protected $table = 'ville';
  protected $primaryKey='id_vil';
  public $timestamps = false;
  public function annonce()
  {
    return $this->belongsTo('Annonce');
  }

}

?>
