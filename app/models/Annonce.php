<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Annonce extends Eloquent {
  protected $table = 'annonce';
  protected $primaryKey='id_ann';
  public $timestamps = false;

  public function vendeur()
  {
    return $this->belongsTo('Vendeur','id_ven');
  }

  public function type()
  {
    return $this->belongsTo('Type','id_typ');
  }
  public function ville()
  {
    return $this->belongsTo('Ville','id_ville');
  }
  public function quartier()
  {
    return $this->belongsTo('Quartier','id_qua');
  }
}

?>
