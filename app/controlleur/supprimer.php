<?php

/***********
Traitement des données pour la suppresion
***********/

if (empty($_POST['supp'])){
  $num = $_POST['id_ann'];
  $sup = Annonce::with('type','ville','vendeur','quartier')->where('id_ann', '=', $num)->get();
  $app->render('supprimer.twig', array('supprimer' => $sup));
}else{
  $pwd = $_POST['pwd'];
  $num = $_POST['id'];
  $sup = Annonce::with('type','ville','vendeur','quartier')->where('id_ann', '=', $num)->get();
  foreach ($sup as $value){
    $mdp = $value['vendeur']['pass_ven'];
  }
  if ($mdp == $pwd){
    $del = Annonce::where('id_ann','=',$num)->delete();
    $app->render('supprimer2.twig',array('sup' => $sup));

  }else{
    $app->render('supprimer3.twig', array('sup' => $num));
  }
}


?>
