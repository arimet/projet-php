<?php

/************************
Traitement des données pour la recherhce

*********************/
$recherche_rez = Annonce::with('type','ville');


if (isset($_POST['categorie'])){
  $categorie = $_POST['categorie'];
  if ($categorie != "all"){
    $recherche_rez = $recherche_rez ->whereHas('type', function($Query) use($categorie) {
      $Query->where('nom_typ', 'LIKE', $categorie);
    });
  }
}
if (isset($_POST['lieu'])){
  $lieu = $_POST['lieu'];
  if ($lieu != "all"){
    $recherche_rez = $recherche_rez ->whereHas('ville', function($Query) use($lieu) {
      $Query->where('nom_vil', 'LIKE', $lieu);
    });
  }
}
if (strlen($_POST['mot_cle']) != 0){
  $mot_cle = $_POST['mot_cle'];
  $recherche_rez = $recherche_rez ->where('desc_ann','LIKE','%'.$mot_cle.'%')->orWhere('titre_ann','LIKE','%'.$mot_cle.'%');
}
if (strlen($_POST['budget_max']) != 0){
  $budget_max = $_POST['budget_max'];
  $recherche_rez = $recherche_rez->where('prix_ann', '<=', $budget_max);
}

if (strlen($_POST['superficie_min']) != 0){
  $superficie_min = $_POST['superficie_min'];
  $recherche_rez = $recherche_rez->where('superficie_ann', '>=', $superficie_min);
}
if (strlen($_POST['nb_piece']) != 0){
  $nb_piece = $_POST['nb_piece'];
  $recherche_rez = $recherche_rez->where('nbpiece_ann', '>=', $nb_piece);
}
if (isset($_POST['tri'])){
  $tri = $_POST['tri'];
  if($tri == "tri2"){
      $recherche_rez = $recherche_rez->orderBy('date_ann','DESC');
  }
  if($tri == "tri1"){
      $recherche_rez = $recherche_rez->orderBy('date_ann','ASC');
  }
  if($tri == "tri4"){
      $recherche_rez = $recherche_rez->orderBy('prix_ann','ASC');
  }
  if($tri == "tri3"){
      $recherche_rez = $recherche_rez->orderBy('prix_ann','DESC');
  }
}


$recherche_rez = $recherche_rez ->get();

?>
