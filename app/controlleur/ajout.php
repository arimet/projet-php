<?php
/****************
Traitement des données vendeur

************/

if (strlen($_POST['nom']) != 0){
  $nom = $_POST['nom'];
}

if (strlen($_POST['prenom']) != 0){
  $prenom = $_POST['prenom'];
}
if (strlen($_POST['mail']) != 0){
  $mail = $_POST['mail'];
}
if (strlen($_POST['num']) != 0){
  $num = $_POST['num'];
}
if (strlen($_POST['adresse']) != 0){
  $adresse = $_POST['adresse'];
}
if (strlen($_POST['type_ven']) != 0){
  $type_ven = $_POST['type_ven'];
}
if (strlen($_POST['pwd']) != 0){
  $pwd = $_POST['pwd'];
}
$vendeur = Vendeur::where('nom_ven','LIKE',$nom)->where('prenom_ven','LIKE',$prenom)->where('mail_adr','LIKE',$mail)->get();
if (strlen($vendeur)<3){
  $vendeur = new Vendeur;
  $vendeur->numTel_ven = $num;
  $vendeur->adresse_Ven = $adresse;
  $vendeur->mail_adr = $mail;
  $vendeur->nom_ven = $nom;
  $vendeur->prenom_ven = $prenom;
  $vendeur->type_ven = $type_ven;
  $vendeur->pass_ven = $pwd;
  $vendeur->save();

  $vendeur = Vendeur::where('nom_ven','LIKE',$nom)->where('prenom_ven','LIKE',$prenom)->where('mail_adr','LIKE',$mail)->get();
}
foreach ($vendeur as $key) {
  $id_ven = $key['id_ven'];
}


/******************
Traitement des données ville
******************/

if (strlen($_POST['ville']) != 0){
  $nom_ville = $_POST['ville'];
}

$ville = Ville::where('nom_vil', 'LIKE', $nom_ville)->get();
if(strlen($ville)<3){
  $ville = new Ville;
  $ville->nom_vil = $nom_ville;
  $ville->save();
  $ville = Ville::where('nom_vil', 'LIKE', $nom_ville)->get();
}
foreach ($ville as $key) {
  $id_ville = $key['id_vil'];
}

/*****************
Traitement des données quatier
*****************/
if (strlen($_POST['quartier']) != 0){
  $nom_quartier = $_POST['quartier'];
}

$quartier = Quartier::where('nom_qua','LIKE',$nom_quartier)->where('id_vil','=',$id_ville)->get();
if(strlen($quartier)<3){
  $quartier = new Quartier;
  $quartier->nom_qua = $nom_quartier;
  $quartier->id_vil = $id_ville;
  $quartier->save();
  $quartier = Quartier::where('nom_qua','LIKE',$nom_quartier)->where('id_vil','=',$id_ville)->get();
}
foreach ($quartier as $key) {
  $id_qua = $key['id_qua'];
}
/*************************
Traitement type bien
**************************/
if (strlen($_POST['categorie']) != 0){
  $nom_type = $_POST['categorie'];
}
$type = Type::where('nom_typ','LIKE',$nom_type)->get();
foreach ($type as $key) {
  $id_typ = $key['id_typ'];
}

/*******************
traitement pour annonce
********************/
if (strlen($_POST['titre']) != 0){
  $titre = $_POST['titre'];
}
if (strlen($_POST['prix']) != 0){
  $prix = $_POST['prix'];
}
if (strlen($_POST['superficie']) != 0){
  $superficie = $_POST['superficie'];
}
if (strlen($_POST['nb_piece']) != 0){
  $nb_piece = $_POST['nb_piece'];
}
if (strlen($_POST['loc_ann']) != 0){
  $loc_ann = $_POST['loc_ann'];
}
if (strlen($_POST['desc_ann']) != 0){
  $desc_ann = $_POST['desc_ann'];
}

/*************
Traitement pour l'images trouvé sur le net et donc pas optimisé
*************/

$dossier = 'images/';
$fichier = basename($_FILES['avatar']['name']);
$taille_maxi = 3000000;
$taille = filesize($_FILES['avatar']['tmp_name']);
$extensions = array('.png', '.gif', '.jpg', '.jpeg');
$extension = strrchr($_FILES['avatar']['name'], '.');
//Début des vérifications de sécurité...
if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
{

}

if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
{
     //On formate le nom du fichier ici...
     $fichier = strtr($fichier,
          'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
          'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
     $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);
     if(move_uploaded_file($_FILES['avatar']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
     {

     }
}




/************
Envoie
***********/

$annonce = new Annonce;
$annonce->titre_ann = $titre;
$annonce->desc_ann = $desc_ann;
$annonce->image_ann = "images/".$fichier;
$annonce->id_qua = $id_qua;
$annonce->superficie_ann = $superficie;
$annonce->prix_ann = $prix;
$annonce->id_ville = $id_ville;
$annonce->id_ven = $id_ven;
$annonce->id_typ = $id_typ;
$annonce->nbpiece_ann = $nb_piece;
$annonce->date_ann = date("Y-m-d");
$annonce->loc_ann = $loc_ann;
$annonce->save();




?>
