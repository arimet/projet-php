# README #

### Membres du groupe ###

* Anthony Rimet
* David Cordier

### Projet ? ###

* Projet réalisé dans le cadre d'un projet de la LP-CISIIE de l'IUT Charlemagne de Nancy. 

* C'est un site d'annonces immobilière qui utilise les technologies suivantes: HTML/CSS/Framework maison/PHP/SLIM/ELOQUENT/TWIG.



### Fonctionnalités Présentes ###


* Ajouts d'annonces
* Suppression d'annonces avec mdp
* Recherches d'annonces
* Visualisation de toutes les annonces
* Visualisation d'une annonce

### Répartition du travail ###

* David s'est occupé de toute la partie conception et rédaction du projet

* Anthony s'est occupé de toute la partie réalisation (code) du projet


### Lien du Projet ###

* https://webetu.iutnc.univ-lorraine.fr/www/rimet1u/PHP/